;;; init.lisp --- Entry point for my StumpWM configuration

;; Copyright © 2013–2021 Victor Santos <vct.santos@protonmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :stumpwm)


(load-module "ttf-fonts")
(load-module "stumptray")
(load-module "swm-bs-colors")
(load-module "swm-battery")

(run-shell-command "picom")
(run-shell-command "klipper")
(run-shell-command "nm-applet")
(run-shell-command "dropbox stop && dropbox start")
(run-shell-command "~/.fehbg")

;;;;;;;;;;;;;;;;;
;; Window Gaps ;;
;;;;;;;;;;;;;;;;;
(load-module "swm-gaps")
(setf swm-gaps:*inner-gaps-size* 8)
(setf swm-gaps:*outer-gaps-size* 8)
(setf swm-gaps:*head-gaps-size* 8)
(if (not swm-gaps:*gaps-on*)
    (swm-gaps:toggle-gaps))

(setf *mouse-focus-policy* :click
      *window-border-style* :thick
      *message-window-gravity* :top-right
      *input-window-gravity* :top-right)
(set-normal-gravity :top) ; top for terminals
(set-maxsize-gravity :center) ; center for floating X apps
(set-transient-gravity :center) ; center for save-as/open popups

(setf *grab-pointer-foreground* (xlib:make-color
                                 :red 0.1
                                 :green 0.25
                                 :blue 0.5))
(setf *grab-pointer-background* (lookup-color
                                 (current-screen) "Red"))
(setf *grab-pointer-character* 56)
(setf *grab-pointer-character-mask* 56)

;;;;;;;;;;;;;;;;
;; Workspaces ;;
;;;;;;;;;;;;;;;;
;; Virtual Desktops or workspaces are called *groups* within StumpWM
(defparameter *vct-workspaces* '("www" "dev" "aux"))
;; group title format
(setf *group-format* "%s [%n] %t ")
;; Change name of the first group to www
(setf (group-name (car (screen-groups (current-screen)))) (car *vct-workspaces*))
;; Create the remaining groups
(mapc (lambda (workspace) (gnewbg-dynamic workspace)) (cdr *vct-workspaces*))

(setf *window-number-map* "1234567890")
(setf *frame-number-map* "1234567890")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Window placement rules ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(clear-window-placement-rules)

(define-frame-preference "www"    
    (0 t t :class "firefox")
  (0 t t :instance "Navigator")
  (0 t t :role "browser"))

(define-frame-preference "dev"
    (0 t t :class "Emacs")
  (0 t t :instance "emacs")
  (0 t t :class "konsole")
  (0 t t :instance "konsole"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Interact with StumpWM ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require :swank)
(swank-loader:init)

(defvar *swank-server-running* nil)

(defun swank-start-server ()
  (and (not *swank-server-running*)
       (swank:create-server :port 4004
                            :style swank:*communication-style*
                            :dont-close t)
       (setf *swank-server-running* t)))

(defun swank-stop-server ()
  (and *swank-server-running*
       (swank:stop-server 4005)
       (setf *swank-server-running* nil)))

(defcommand start-swank-server () ()
  "Start swank server."
  (swank-start-server))

(defcommand stop-swank-server () ()
  "Stop swank server."
  (swank-stop-server))

;; start server
(swank-start-server)


(xft:cache-fonts)
(setf xft:*font-dirs* '("/usr/share/fonts" "/home/vct/.local/share/fonts/"))
(set-font (make-instance 'xft:font
                         :family "Noto Sans Mono"
                         :subfamily "Regular" :size 12))

(setf *colors* `("#1d1f21" ; black   (background)
                 "#cc6666" ; red
                 "#b5bd68" ; green
                 "#f0c674" ; yellow
                 "#81a2be" ; blue
                 "#b294bb" ; magenta (purple)
                 "#8abeb7" ; cyan    (aqua)
                 "#c5c8c6" ; white   (foreground)

                 "#969896" ; gray    (comment)
                 "#282a2e" ; (current line)
                 "#373b41" ; (selection)
                 "#de935f" ; (orange)
                 ))

;;;;;;;;;;;;;;
;; Modeline ;;
;;;;;;;;;;;;;;
;;; Mode line's contents
;; (defmacro modeline/shell-cmd (cmd &key (prefix "") (affix ""))
;;   "Runs SHELL-CMD-NEOL but wraps it into the '(:EVAL) which enables refreshing
;; in mode line. Takes the same arguments as SHELL-CMD-NEOL."  
;;   `'(:eval (shell-cmd-neol ,cmd :prefix ,prefix :affix ,affix)))

;; (defmacro modeline/shell-cmd (cmd)
;;   "Runs SHELL-CMD-NEOL but wraps it into the '(:EVAL) which enables refreshing
;; in mode line. Takes the same arguments as SHELL-CMD-NEOL."  
;;   `'(:eval (run-shell-command ,cmd)))

(defun vct/fmt-color (str &key (bg 'dark) (fg 'light))
  (format nil
          "^[^(:fg \"~a\")^(:bg \"~a\") ~a ^]"
          (gethash fg swm-bs-colors::*bs-colors*)
          (gethash bg swm-bs-colors::*bs-colors*)
          str))


;;(stumpwm:run-shell-command "date" t)
;; (defun vct/show-battery-state ()
;;   (let ((battery-percentage (with-input-from-string (in (run-shell-command "acpitool -b | cut -d: -f2 | cut -d, -f2 | cut -d'%' -f1" t)) (read in)))
;;         (on-ac (if (string= "online" (string-trim " " (substitute #\Space #\Newline (run-shell-command "acpitool -a | cut -d: -f2 | sed 's/^ *//g'" t)))) t nil))
;;         (fmt "~a~,1f%"))
;;     (let ((fmt-string (cond ((> battery-percentage 65) (vct/fmt-color fmt :bg 'success :fg 'dark))
;;           ((and (> battery-percentage 20) (< 65)) (vct/fmt-color fmt :bg 'warning :fg 'dark))
;;           ((< battery-percentage 20) (vct/fmt-color fmt :bg 'danger :fg 'dark))
;;           (t (vct/fmt-color fmt :bg 'dark :fg 'light)))))
;;       (format nil fmt-string (if on-ac "(AC) " "") battery-percentage))))

(defun vct/show-workspace ()
  (format nil
          "^[^(:fg \"~a\")^(:bg \"~a\") %n ^]"
          (swm-bs-colors::get-bs-color :light)
          (swm-bs-colors::get-bs-color :primary)))

(setf *mode-line-highlight-template*
      (format nil "^[^(:fg \"~a\")^(:bg \"~a\") ~~A ^]"
              (swm-bs-colors::get-bs-color :light)
              (swm-bs-colors::get-bs-color :secondary)))

;;(setf *time-format-string-default* "%a %b %e %Y %k:%M")
(setf *time-modeline-string* "%a %e/%m/%Y %k:%M")
(setf *window-format* (format nil "%n[%s] %20t"))

(setf *screen-mode-line-format* (list
                                 '(:eval (vct/show-workspace))
                                 "%B"
                                 " %d"
                                 " | "
                                 ;; "^>"
                                 "%v" ))
(setf *mode-line-timeout* 1)
(setf *mode-line-position* :bottom)
(setf *mode-line-timeout* 1)
(setf *mode-line-foreground-color* (nth 8 *colors*))
(setf *mode-line-background-color* (nth 0 *colors*))
(setf *mode-line-border-width* 0)
(setf *mode-line-pad-y* 7)

(unless (head-mode-line (current-head))
  (toggle-mode-line (current-screen) (current-head)))

;; System Tray

(defvar *rc-stumptray-enabled* nil)
(unless *rc-stumptray-enabled*
  (setf *rc-stumptray-enabled* t)
  ;;(setf stumptray:*tray-placeholder-pixels-per-space* 100)
  (stumptray:stumptray))

(load-module "kbd-layouts")
(kbd-layouts::keyboard-layout-list "us -variant intl" "br")

(defcommand web-browser () ()
            "run web browser"
            (run-or-raise "firefox-bin" '(:class "Firefox")))

(define-key *root-map* (kbd "B") "web-browser")

(defcommand terminal () ()
            "run terminal"
            (run-or-raise "konsole" '(:class "Konsole")))

(defcommand xterm () ()
            "run terminal"
            (run-or-raise "xterm" '(:class "XTerm")))

(defun rofi (mode)
  (run-shell-command (concat "rofi -show " mode " -m " (write-to-string (head-number (current-head))))))

(defcommand apps () () (rofi "drun"))
;;(defcommand rofi-run () () (rofi "run"))

(defcommand reinit () ()
  "reinit"
  (run-commands "reload" "loadrc"))
